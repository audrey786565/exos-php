
<?php

//Exo 1
$mois = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'décembre'];

//Exo 2
$mois = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'décembre'];
echo $mois[2];

//Exo 3
$mois = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'décembre'];
echo $mois[5];

//Exo 4
$mois = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'décembre'];
$mois['aout'] = 'août';
print_r($mois);


//Exo 5
$departements = [
    02 => 'Aisne',
    60 => 'Oise',
    62 => 'Pas-de-Calais',
    80 => 'Somme'
];

/*Exo 6
J'ai pas compris cet exo, l'index 59 n'existe pas ici?*/

//Exo 7
$departements = [
    02 => 'Aisne',
    59 => 'Nord',
    60 => 'Oise',
    62 => 'Pas-de-Calais',
    80 => 'Somme'
];
$departements += [51 => 'Reims'];
print_r($departements);

//Exo 8
$mois = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'décembre'];
foreach ($mois as $mois) {
    echo "-$mois \n";
}

//Exo 9
$departements = [
    '02' => 'Aisne',
    60 => 'Oise',
    62 => 'Pas-de-Calais',
    80 => 'Somme'
];
foreach ($departements as $departement => $ville) {
    echo "$departement => $ville \n";
}


//Exo 10
$departements = [
    02 => 'Aisne',
    60 => 'Oise',
    62 => 'Pas-de-Calais',
    80 => 'Somme'
];
foreach ($departements as $departement => $ville) {
    echo "Le numéro de département de la ville $ville est le $departement \n";
}
?>
