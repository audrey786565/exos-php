<?php

//Exo 1
$variable = null;
for ($i = 0; $i <= 10; $i++) {
    echo "- $i \n";
}


//Exo 2
$var1 = 0;
$var2 = 30;

while ($var1 < 20) {
  $var1++;
  $var1 * $var2;
  echo  $var1 * $var2;
}

//Revue de code : 
//Comment afficher les résultats les uns en dessous des autres?


//Exo 3 
$var1 = 100;
$var2 = 50;

while ($var1 >= 10) {
  $var1--;
  $var1 * $var2;
  echo $var1 * $var2;

}

//ou

$var1 = 100;
$var2 = 50;

for ($var1 = 100; $var1 >= 10; $var1--) {
  echo $var1 * $var2;
}

//Revue de code :
// Lequel est bon : exo 1 commence à 4950 et finit à 450 ; exo 2 commence à 5000 et finit à 500


//Exo 4 Pas réussi
$valeur = 1;
$resultat = ($valeur/2);
for ($valeur = 1; $valeur = 10; $resultat++) {
  echo $resultat;
}


$valeur = 1;
$resultat = $valeur / 2;
while ($valeur = 10) {
  $valeur++;

  echo $resultat;
}


//Exo 5
$var = 1;

for ($var = 1; $var <= 15; $var++) {
  if($var <= 15) {
    echo "$var On y arrive presque \n";
    }
  }
  

  //Exo 6
  $var = 20;

  for ($var = 20; $var >= 0; $var--) {
    if($var >= 0) {
      echo "$var C'est presque bon \n";
    }
  }
  

  //Exo 7
  $var = 1;

  for ($var = 1; $var <= 100; $var += 15) {
    if($var <= 100) {
      echo "$var On tient le bon bout \n";
    }
  }


  //Exo 8
  $var = 200;

  for ($var = 200; $var >= 0; $var -= 12) {
    if($var >= 0) {
      echo "$var Enfin !!!! \n"; 
    }
  }

?>